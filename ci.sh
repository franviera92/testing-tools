#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset

CONVERT_REPORT='([.resource_changes[]?.change.actions?]|flatten)|{"create":(map(select(.=="create"))|length),"update":(map(select(.=="update"))|length),"delete":(map(select(.=="delete"))|length)}'
PLAN=plan.tfplan
DESTROY_PLAN=destroy_plan.tfplan
PLAN_JSON=${PLAN_JSON:-tfplan.json}
TF_PARALLELISM=${TF_PARALLELISM:-3}
TF_BIN=terraform
#comment
function configure_environment {
    if [[ ! -z ${CI_COMMIT_TAG:-} ]]; then
        STAGE=prod
    elif [[ ${CI_COMMIT_REF_NAME:-} == "master" ]]; then
        STAGE=qa
    else
        # STAGE=dev
        STAGE=${CI_COMMIT_REF_NAME:-}
    fi
    export ENVIRONMENT=$(echo ${STAGE:-local}| tr '[:upper:]' '[:lower:]')
    if [[ ${ENVIRONMENT} == 'local' ]]; then
        TERRAFORM_PREFIX=""
    else
        TERRAFORM_PREFIX="${ENVIRONMENT}_"
    fi
    echo "Trabajando en: "
    echo ${ENVIRONMENT} "stage"
}

function configure_kubeconfig {
    if [[ ${ENVIRONMENT} == 'local' ]]; then
        echo prefix $TERRAFORM_PREFIX
        KUBECONFIG=$(eval echo \$kubeconfig)
    else
        KUBECONFIG=$(eval echo "\$${TERRAFORM_PREFIX}kubeconfig")
    fi
    export KUBECONFIG=${KUBECONFIG}
    echo "Usando kubeconfig:" $KUBECONFIG
}

function configure_terraform_envs {
    for var in $@; do
        if [[ ! ${var} =~ "ldap" ]]; then
            if [[ -n "$(eval echo "\$${TERRAFORM_PREFIX}${var}")" ]]; then
                export TF_VAR_${var}=$(eval echo "\$${TERRAFORM_PREFIX}${var}")
            else
                echo WARNING: ${TERRAFORM_PREFIX}${var} no está definida como variable de entorno, validar si usará el valor por defecto.
            fi
        fi
    done
}

function configure_ldap {
    if [[ -n "$(eval echo "\$${TERRAFORM_PREFIX}enable_ldap")" ]]; then
        export TF_VAR_enable_ldap="$(eval echo "\$${TERRAFORM_PREFIX}enable_ldap")"
    fi
    for var in $@; do
        if [[ ${var} =~ "ldap" && ${var} != "enable_ldap" ]]; then
            if [[ ${TF_VAR_enable_ldap} == 'true' && -n "$(eval echo "\$${TERRAFORM_PREFIX}${var}")" ]]; then
                export TF_VAR_${var}=$(eval echo "\$${TERRAFORM_PREFIX}${var}")
            elif [[ ${TF_VAR_enable_ldap} == 'true' && -z "$(eval echo "\$${TERRAFORM_PREFIX}${var}")" ]]; then
                echo WARNING: ${TERRAFORM_PREFIX}${var} no está definida como variable de entorno, validar si usará el valor por defecto.
            else
                export TF_VAR_${var}="notused"
            fi
        fi
    done
}

all_variables=(
    "$(grep "variable " vars.tf |awk '{print $2}')"
)

configure_environment
configure_kubeconfig
configure_terraform_envs "${all_variables[@]}"
configure_ldap "${all_variables[@]}"

function terraform_apply {
    $TF_BIN apply ${PLAN}
}

function terraform_destroy {
    $TF_BIN apply ${DESTROY_PLAN}
}

function terraform_init {
    $TF_BIN init
    $TF_BIN workspace select $ENVIRONMENT || $TF_BIN workspace new $ENVIRONMENT
}

function terraform_plan {
    $TF_BIN plan -out=${PLAN} -parallelism=$TF_PARALLELISM
    $TF_BIN show --json ${PLAN}|jq -r ${CONVERT_REPORT} > ${PLAN_JSON}
}

function terraform_plan_destroy {
    $TF_BIN plan -destroy -out=${DESTROY_PLAN} -parallelism=$TF_PARALLELISM
}

function terraform_refresh {
    $TF_BIN refresh
}

function terraform_validate {
    $TF_BIN validate
}

$1